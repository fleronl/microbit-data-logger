# coding: utf-8
"""
__authors__     = 'Laura Fléron'
__contact__     = 'laura.fleron@ac-reims.fr'
__copyleft__    = 'LGPL'
__date__        = '2021.12.28'
__version__     = '1.4'

Exemple 
Donne le port serie de a Micro:bit
    print(f' La micro:bit est là : {microfs.get_serial()}')
    
Lister tous les fichiers contenus sur la micro:bit
    print(microfs.ls())
    
Dépôt sur la micro:bit
    microfs.put("mon_fichier.csv")
"""

import microfs
import matplotlib.pyplot as plt

def recupere_fichier():
    """Récupère tous les fichiers csv contenus dans la microBit"""
    
    print('Liste des fichiers csv sur la carte microBit :')
    for element in microfs.ls():
        if element.split(".")[1] == "csv":
            print(element)
            microfs.get(element)

def importe_trace():
    """ trace le fichier demandé"""
    fichier = input('Quel fichier ? :')
    
    if fichier not in microfs.ls():
        print(f"Le fichier {fichier} n'est pas présent sur la carte !")
        
    else :
        microfs.get(fichier)
        x, y = list(), list()

        with open(fichier, 'r') as f:
            for line in f:
                line = line.strip()
                line = list(line.split(';'))
                x.append(float(line[0]))
                y.append(float(line[1]))
            #print(x, y)
            
        fig, ax = plt.subplots()
        ax.plot(x, y,
                color='blue',
                marker='x',
                linestyle='dashed',
                linewidth=1,
                markersize=5)

        ax.set(xlabel='Période (s)',
               ylabel='Température (°c)',
               title='Fichier '+fichier)

        ax.grid()
        fig.savefig("test.png")
        plt.show()

if __name__ == "__main__" :
    """ Lance les fonctions à l'execution du fichier"""
    print('trace une courbe')
    recupere_fichier()
    importe_trace()
