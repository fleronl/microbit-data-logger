# coding: utf-8

"""
__authors__     = 'Laura Fléron'
__contact__     = 'laura.fleron@ac-reims.fr'
__copyleft__    = 'LGPL'
__date__        = '2021.12.28'
__version__     = '1.4'
"""

from microbit import *
import time
import os

def creation_fichier()->str :
    """ Recherche et création d'un fichier type xxx.cxv """

    fichiers = os.listdir()
    fichier = 1
    boucle = True

    while boucle == True and fichier < 50 :

        recherche = '0'+str(fichier)
        recherche = recherche[-2:] + '.csv'

        if recherche in fichiers:
            fichier += 1
        else:
            boucle = False

    if fichier == 50 :
        fin_mesure()

    display.on()
    display.scroll(str(recherche[:2]))
    display.clear()
    display.off()

    return recherche

def attente_mesure()->None :
    """ Attente d'une prise de mesures """

    while 1:
        if button_a.is_pressed():
            display.on()
            display.show(Image.ARROW_E)
            sleep(500)
            display.clear()
            display.off()
            return None

def prise_mesure(fichier, pas=1, iteration=100)->None :
    """
    Pise de mesures
        pas = seconde
        iteration = nb de valeurs
    """
    duree_ms = pas * 1000
    boucle = True
    valeur = 1

    try :
        file = open(fichier, 'w')
    except :
        print("erreur d'ouverture")
    else :
        start = time.ticks_ms()

        while boucle == True :

            if valeur > iteration :
                boucle = False
                file.close()

            if button_b.is_pressed() :
                boucle = False
                file.close()

            lapse_temps = time.ticks_diff(time.ticks_ms(), start)

            if ( lapse_temps > duree_ms and boucle == True ) :
                tempe = temperature()
                print((tempe,))
                temps = valeur * pas
                print(temps)
                message = str(temps) + ';' + str(tempe) + '\n'
                file.write(message)
                sleep(5)
                start = time.ticks_ms()
                valeur += 1

                display.on()
                display.show(Image.ARROW_S)
                sleep(200)
                display.clear()
                display.off()


def fin_mesure()->None :
    display.on()
    display.scroll('Fin')
    sleep(500)
    display.off()

if __name__ == '__main__' :

    PAS = 1             # en seconde
    ITERATION = 20      # nombre total de boucles

    fichier = creation_fichier()
    attente_mesure()    # Appui sur le bouton A
    prise_mesure(fichier, PAS, ITERATION)
    fin_mesure()        # Appui sur le bouton B
